#!/usr/local/bin/python3

import collections

n=10
with open("masterlist.txt", encoding = "ISO-8859-1") as f:
    # notice there is "encoding = "ISO-8859-1", this is for Python3 compatibility.
    # you can remove it for if you have Python2.x. Well actully, in the password list,
    # there is a password with a byte character: 0x82 => "‚".
    content =f.readlines()

content = [x.strip() for x in content]

print (collections.Counter(content).most_common(n))
